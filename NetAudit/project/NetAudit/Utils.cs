﻿namespace NetAudit {
    internal class Utils {
        public static string FormatSpeed(long bitsPerSecond) {
            if (bitsPerSecond > 1000000) {
                return $"≈ {bitsPerSecond / 1000000} Mbit/s";
            }

            return bitsPerSecond > 1000 ? $"≈ {bitsPerSecond / 1000} kbit/s" : $"{bitsPerSecond} bit/s";
        }
    }
}