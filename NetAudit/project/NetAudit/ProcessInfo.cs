namespace NetAudit {
    internal class ProcessInfo {
        public int Id { get; set; }
        public string Name { get; set; }
    };
}