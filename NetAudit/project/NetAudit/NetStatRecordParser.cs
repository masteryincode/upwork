﻿using System.Text.RegularExpressions;

namespace NetAudit {
    internal class NetStatRecordParser : IRecordParser<NetStatRecord> {
        private const string Host = @"(?:\[[^\[\]]+\])|(?:[^\[\]\:]+)";
        private const string TcpStateRxString = @"(?<TcpState>\S+?)";
        private const string PidRxString = @"(?<Pid>\d+)";

        private static readonly string LocalAddressRxString =
            $@"(?<LocalAddress>(?<LocalAddressHost>{Host}):(?<LocalAddressPort>\S+?))";

        private static readonly string ForeignAddressRxString =
            $@"(?<ForeignAddress>(?<ForeignAddressHost>{Host}):(?<ForeignAddressPort>\S+?))";

        public NetStatRecord Parse(string s) {
            var trimmed = s.Trim();

            var tcpNetStatRecordWithPid =
                new Regex(
                    $@"TCP\s+{LocalAddressRxString}\s+{ForeignAddressRxString}\s+{TcpStateRxString}\s+{PidRxString}");
            var tcpNetStatRecordWithoutPid =
                new Regex($@"TCP\s+{LocalAddressRxString}\s+{ForeignAddressRxString}\s+{TcpStateRxString}");
            var udpNetStatRecordWithPid =
                new Regex($@"UDP\s+{LocalAddressRxString}\s+{ForeignAddressRxString}\s+{PidRxString}");
            var udpNetStatRecordWithoutPid =
                new Regex($@"UDP\s+{LocalAddressRxString}\s+{ForeignAddressRxString}");

            if (trimmed.Length == 0) {
                return NetStatRecord.EmptyRecord;
            }

            var tcpNetStatRecordWithPidMatch = tcpNetStatRecordWithPid.Match(trimmed);

            if (tcpNetStatRecordWithPidMatch.Success) {
                return new NetStatRecord {
                    RecordType = NetStatRecord.RecordTypeEnum.WithPid,
                    Protocol = NetStatRecord.ProtocolEnum.Tcp,
                    LocalAddress = tcpNetStatRecordWithPidMatch.Groups["LocalAddress"].Value,
                    ForeignAddress = tcpNetStatRecordWithPidMatch.Groups["ForeignAddress"].Value,
                    TcpState = NetStatRecord.StringToTcpStateEnum[tcpNetStatRecordWithPidMatch.Groups["TcpState"].Value],
                    Pid = long.Parse(tcpNetStatRecordWithPidMatch.Groups["Pid"].Value)
                };
            }

            var tcpNetStatRecordWithoutPidMatch = tcpNetStatRecordWithoutPid.Match(trimmed);

            if (tcpNetStatRecordWithoutPidMatch.Success) {
                return new NetStatRecord {
                    RecordType = NetStatRecord.RecordTypeEnum.WithoutPid,
                    Protocol = NetStatRecord.ProtocolEnum.Tcp,
                    LocalAddress = tcpNetStatRecordWithoutPidMatch.Groups["LocalAddress"].Value,
                    ForeignAddress = tcpNetStatRecordWithoutPidMatch.Groups["ForeignAddress"].Value,
                    TcpState =
                        NetStatRecord.StringToTcpStateEnum[tcpNetStatRecordWithoutPidMatch.Groups["TcpState"].Value]
                };
            }

            var udpNetStatRecordWithPidMatch = udpNetStatRecordWithPid.Match(trimmed);

            if (udpNetStatRecordWithPidMatch.Success) {
                return new NetStatRecord {
                    RecordType = NetStatRecord.RecordTypeEnum.WithPid,
                    Protocol = NetStatRecord.ProtocolEnum.Udp,
                    LocalAddress = udpNetStatRecordWithPidMatch.Groups["LocalAddress"].Value,
                    ForeignAddress = udpNetStatRecordWithPidMatch.Groups["ForeignAddress"].Value,
                    Pid = long.Parse(udpNetStatRecordWithPidMatch.Groups["Pid"].Value)
                };
            }

            var udpNetStatRecordWithoutPidMatch = udpNetStatRecordWithoutPid.Match(trimmed);

            if (udpNetStatRecordWithoutPidMatch.Success) {
                return new NetStatRecord {
                    RecordType = NetStatRecord.RecordTypeEnum.WithoutPid,
                    Protocol = NetStatRecord.ProtocolEnum.Udp,
                    LocalAddress = udpNetStatRecordWithoutPidMatch.Groups["LocalAddress"].Value,
                    ForeignAddress = udpNetStatRecordWithoutPidMatch.Groups["ForeignAddress"].Value
                };
            }


            return new NetStatRecord {RecordType = NetStatRecord.RecordTypeEnum.Info, Info = s};
        }
    }
}