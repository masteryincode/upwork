using System.Collections.Generic;

namespace NetAudit {
    internal class ProcessInfoEqualityComparer : IEqualityComparer<ProcessInfo> {
        public static readonly ProcessInfoEqualityComparer Instance = new ProcessInfoEqualityComparer();

        public bool Equals(ProcessInfo x, ProcessInfo y) {
            return x.Id == y.Id && x.Name.Equals(y.Name);
        }

        public int GetHashCode(ProcessInfo p) {
            return (p.Id * 397) ^ p.Name.GetHashCode();
        }
    }
}