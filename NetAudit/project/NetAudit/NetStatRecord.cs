﻿using System.Collections.Generic;

namespace NetAudit {
    internal class NetStatRecord {
        public enum ProtocolEnum {
            Tcp,
            Udp
        }

        public enum RecordTypeEnum {
            Empty,
            Info,
            WithoutPid,
            WithPid
        }

        public enum TcpStateEnum {
            Listening,
            Established,
            TimeWait,
            SynSent,
            Closed,
            SynReceived,
            CloseWait,
            FinWait1,
            Closing,
            LastAck,
            FinWait2
        }

        public static NetStatRecord EmptyRecord = new NetStatRecord {RecordType = RecordTypeEnum.Empty};

        public static Dictionary<string, ProtocolEnum> StringToProtocolEnum = new Dictionary<string, ProtocolEnum> {
            {@"TCP", ProtocolEnum.Tcp},
            {@"UDP", ProtocolEnum.Udp}
        };

        public static Dictionary<string, TcpStateEnum> StringToTcpStateEnum = new Dictionary<string, TcpStateEnum> {
            {@"CLOSED", TcpStateEnum.Closed},
            {@"LISTEN", TcpStateEnum.Listening},
            {@"LISTENING", TcpStateEnum.Listening},
            {@"SYN_SENT", TcpStateEnum.SynSent},
            {@"SYN_RECEIVED", TcpStateEnum.SynReceived},
            {@"ESTABLISHED", TcpStateEnum.Established},
            {@"CLOSE_WAIT", TcpStateEnum.CloseWait},
            {@"FIN_WAIT_1", TcpStateEnum.FinWait1},
            {@"CLOSING", TcpStateEnum.Closing},
            {@"LAST_ACK", TcpStateEnum.LastAck},
            {@"FIN_WAIT_2", TcpStateEnum.FinWait2},
            {@"TIME_WAIT", TcpStateEnum.TimeWait}
        };

        public RecordTypeEnum RecordType { get; set; }
        public string Info { get; set; }
        public ProtocolEnum Protocol { get; set; }
        public string LocalAddress { get; set; }
        public string ForeignAddress { get; set; }
        public TcpStateEnum TcpState { get; set; }
        public long Pid { get; set; }
    }
}