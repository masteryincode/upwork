﻿using System.Collections.Generic;
using System.Linq;

namespace NetAudit {
    internal class NetCommand {
        public static Builder PingBuilder = new Builder("ping");
        public static Builder WhoisBuilder = new Builder("whois"); //.AddArgs("domain");
        public static Builder TracerouteBuilder = new Builder("tracert");
        public static NetCommand InspectTrafficCommand = new Builder("netstat").AddArgs("-b").Build();
        public static NetCommand NetStatCommand = new Builder("netstat").AddArgs("-a", "-o").Build();

        public HashSet<string> Args { get; private set; }
        public string Executable { get; private set; }
        public string JoinedArgs => string.Join(" ", Args.ToArray());

        public class Builder {
            public Builder(string executable) {
                Executable = executable;
                Args = new HashSet<string>();
            }

            private string Executable { get; }
            private HashSet<string> Args { get; set; }

            public Builder AddArgs(params string[] args) {
                return new Builder(Executable) {Args = new HashSet<string>(Args.Union(args))};
            }

            public NetCommand Build() {
                return new NetCommand {Executable = Executable, Args = new HashSet<string>(Args)};
            }
        }
    }
}