﻿namespace NetAudit
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.processSearchTextBox = new System.Windows.Forms.TextBox();
            this.searchProcessLabel = new System.Windows.Forms.Label();
            this.processesListView = new System.Windows.Forms.ListView();
            this.PIDHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.usageProgressBar = new System.Windows.Forms.ProgressBar();
            this.currentSpeedLabel = new System.Windows.Forms.Label();
            this.saveOutputCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.inspectTrafficButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.urlTextInput = new System.Windows.Forms.TextBox();
            this.pingButton = new System.Windows.Forms.Button();
            this.tracerouteButton = new System.Windows.Forms.Button();
            this.whoisButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.receivedLabel = new System.Windows.Forms.Label();
            this.sentLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.logListBox = new System.Windows.Forms.ListBox();
            this.linkLabel = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.workProgressBar = new System.Windows.Forms.ProgressBar();
            this.stopButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.86391F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.13609F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.processesListView, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.linkLabel, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 468);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to NetAudit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.processSearchTextBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.searchProcessLabel, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(288, 60);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // processSearchTextBox
            // 
            this.processSearchTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.processSearchTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.processSearchTextBox.Location = new System.Drawing.Point(3, 37);
            this.processSearchTextBox.Name = "processSearchTextBox";
            this.processSearchTextBox.Size = new System.Drawing.Size(282, 20);
            this.processSearchTextBox.TabIndex = 0;
            // 
            // searchProcessLabel
            // 
            this.searchProcessLabel.AutoSize = true;
            this.searchProcessLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.searchProcessLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.searchProcessLabel.Location = new System.Drawing.Point(3, 18);
            this.searchProcessLabel.Name = "searchProcessLabel";
            this.searchProcessLabel.Size = new System.Drawing.Size(282, 16);
            this.searchProcessLabel.TabIndex = 2;
            this.searchProcessLabel.Text = "Search Process";
            this.searchProcessLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // processesListView
            // 
            this.processesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PIDHeader,
            this.NameHeader});
            this.processesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processesListView.FullRowSelect = true;
            this.processesListView.GridLines = true;
            this.processesListView.Location = new System.Drawing.Point(3, 89);
            this.processesListView.Name = "processesListView";
            this.processesListView.Size = new System.Drawing.Size(288, 356);
            this.processesListView.TabIndex = 4;
            this.processesListView.UseCompatibleStateImageBehavior = false;
            this.processesListView.View = System.Windows.Forms.View.Details;
            // 
            // PIDHeader
            // 
            this.PIDHeader.Text = "PID";
            this.PIDHeader.Width = 71;
            // 
            // NameHeader
            // 
            this.NameHeader.Text = "Name";
            this.NameHeader.Width = 175;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(713, 89);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.20388F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.79612F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(168, 356);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.usageProgressBar, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.currentSpeedLabel, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.saveOutputCheckBox, 0, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.03883F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.96117F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(162, 147);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(3, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Network Usage";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // usageProgressBar
            // 
            this.usageProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usageProgressBar.Location = new System.Drawing.Point(3, 35);
            this.usageProgressBar.Maximum = 1000;
            this.usageProgressBar.Name = "usageProgressBar";
            this.usageProgressBar.Size = new System.Drawing.Size(156, 64);
            this.usageProgressBar.Step = 1;
            this.usageProgressBar.TabIndex = 1;
            this.usageProgressBar.Value = 10;
            // 
            // currentSpeedLabel
            // 
            this.currentSpeedLabel.AutoSize = true;
            this.currentSpeedLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.currentSpeedLabel.Location = new System.Drawing.Point(3, 102);
            this.currentSpeedLabel.Name = "currentSpeedLabel";
            this.currentSpeedLabel.Size = new System.Drawing.Size(156, 13);
            this.currentSpeedLabel.TabIndex = 3;
            this.currentSpeedLabel.Text = "label6";
            this.currentSpeedLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // saveOutputCheckBox
            // 
            this.saveOutputCheckBox.AutoSize = true;
            this.saveOutputCheckBox.Checked = true;
            this.saveOutputCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveOutputCheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.saveOutputCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveOutputCheckBox.Location = new System.Drawing.Point(3, 125);
            this.saveOutputCheckBox.Name = "saveOutputCheckBox";
            this.saveOutputCheckBox.Size = new System.Drawing.Size(99, 19);
            this.saveOutputCheckBox.TabIndex = 4;
            this.saveOutputCheckBox.Text = "save output";
            this.saveOutputCheckBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.inspectTrafficButton, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.urlTextInput, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.pingButton, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tracerouteButton, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.whoisButton, 0, 4);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 156);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 6;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(162, 197);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // inspectTrafficButton
            // 
            this.inspectTrafficButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inspectTrafficButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.inspectTrafficButton.Location = new System.Drawing.Point(1, 168);
            this.inspectTrafficButton.Margin = new System.Windows.Forms.Padding(1);
            this.inspectTrafficButton.Name = "inspectTrafficButton";
            this.inspectTrafficButton.Size = new System.Drawing.Size(160, 28);
            this.inspectTrafficButton.TabIndex = 5;
            this.inspectTrafficButton.Tag = "task";
            this.inspectTrafficButton.Text = "Inspect Traffic";
            this.inspectTrafficButton.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(3, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "URL";
            // 
            // urlTextInput
            // 
            this.urlTextInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.urlTextInput.Location = new System.Drawing.Point(3, 54);
            this.urlTextInput.Name = "urlTextInput";
            this.urlTextInput.Size = new System.Drawing.Size(156, 20);
            this.urlTextInput.TabIndex = 1;
            this.urlTextInput.Text = "8.8.8.8";
            // 
            // pingButton
            // 
            this.pingButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pingButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pingButton.Location = new System.Drawing.Point(1, 78);
            this.pingButton.Margin = new System.Windows.Forms.Padding(1);
            this.pingButton.Name = "pingButton";
            this.pingButton.Size = new System.Drawing.Size(160, 28);
            this.pingButton.TabIndex = 2;
            this.pingButton.Tag = "task";
            this.pingButton.Text = "Ping";
            this.pingButton.UseVisualStyleBackColor = true;
            // 
            // tracerouteButton
            // 
            this.tracerouteButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracerouteButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tracerouteButton.Location = new System.Drawing.Point(1, 108);
            this.tracerouteButton.Margin = new System.Windows.Forms.Padding(1);
            this.tracerouteButton.Name = "tracerouteButton";
            this.tracerouteButton.Size = new System.Drawing.Size(160, 28);
            this.tracerouteButton.TabIndex = 3;
            this.tracerouteButton.Tag = "task";
            this.tracerouteButton.Text = "Traceroute";
            this.tracerouteButton.UseVisualStyleBackColor = true;
            // 
            // whoisButton
            // 
            this.whoisButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.whoisButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.whoisButton.Location = new System.Drawing.Point(1, 138);
            this.whoisButton.Margin = new System.Windows.Forms.Padding(1);
            this.whoisButton.Name = "whoisButton";
            this.whoisButton.Size = new System.Drawing.Size(160, 28);
            this.whoisButton.TabIndex = 4;
            this.whoisButton.Tag = "task";
            this.whoisButton.Text = "Whois";
            this.whoisButton.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.logListBox, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(297, 23);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel7, 2);
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(402, 422);
            this.tableLayoutPanel7.TabIndex = 8;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.receivedLabel, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.sentLabel, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 69.81132F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30.18868F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(396, 84);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // receivedLabel
            // 
            this.receivedLabel.AutoSize = true;
            this.receivedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receivedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.receivedLabel.Location = new System.Drawing.Point(201, 58);
            this.receivedLabel.Name = "receivedLabel";
            this.receivedLabel.Size = new System.Drawing.Size(192, 26);
            this.receivedLabel.TabIndex = 3;
            this.receivedLabel.Text = "Bytes received";
            this.receivedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sentLabel
            // 
            this.sentLabel.AutoSize = true;
            this.sentLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sentLabel.Location = new System.Drawing.Point(3, 58);
            this.sentLabel.Name = "sentLabel";
            this.sentLabel.Size = new System.Drawing.Size(192, 26);
            this.sentLabel.TabIndex = 2;
            this.sentLabel.Text = "Bytes sent";
            this.sentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Image = global::NetAudit.Properties.Resources.down;
            this.label4.Location = new System.Drawing.Point(201, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 58);
            this.label4.TabIndex = 1;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Image = global::NetAudit.Properties.Resources.up;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(192, 58);
            this.label3.TabIndex = 0;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(396, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Networking Statistics";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // logListBox
            // 
            this.logListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logListBox.FormattingEnabled = true;
            this.logListBox.HorizontalScrollbar = true;
            this.logListBox.Location = new System.Drawing.Point(1, 126);
            this.logListBox.Margin = new System.Windows.Forms.Padding(1);
            this.logListBox.Name = "logListBox";
            this.logListBox.Size = new System.Drawing.Size(400, 295);
            this.logListBox.TabIndex = 6;
            // 
            // linkLabel
            // 
            this.linkLabel.AutoSize = true;
            this.linkLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.linkLabel.Location = new System.Drawing.Point(809, 448);
            this.linkLabel.Name = "linkLabel";
            this.linkLabel.Size = new System.Drawing.Size(72, 20);
            this.linkLabel.TabIndex = 9;
            this.linkLabel.TabStop = true;
            this.linkLabel.Text = "AGR Systems";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.workProgressBar, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.stopButton, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(713, 23);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(168, 60);
            this.tableLayoutPanel8.TabIndex = 10;
            // 
            // workProgressBar
            // 
            this.workProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workProgressBar.Location = new System.Drawing.Point(3, 3);
            this.workProgressBar.MarqueeAnimationSpeed = 0;
            this.workProgressBar.Name = "workProgressBar";
            this.workProgressBar.Size = new System.Drawing.Size(162, 24);
            this.workProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.workProgressBar.TabIndex = 0;
            // 
            // stopButton
            // 
            this.stopButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(3, 33);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(162, 24);
            this.stopButton.TabIndex = 1;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 468);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NetAudit";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox processSearchTextBox;
        private System.Windows.Forms.Label searchProcessLabel;
        private System.Windows.Forms.ListView processesListView;
        private System.Windows.Forms.ColumnHeader PIDHeader;
        private System.Windows.Forms.ColumnHeader NameHeader;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label receivedLabel;
        private System.Windows.Forms.Label sentLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar usageProgressBar;
        private System.Windows.Forms.Label currentSpeedLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox urlTextInput;
        private System.Windows.Forms.Button pingButton;
        private System.Windows.Forms.Button tracerouteButton;
        private System.Windows.Forms.Button whoisButton;
        private System.Windows.Forms.Button inspectTrafficButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox logListBox;
        private System.Windows.Forms.LinkLabel linkLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.ProgressBar workProgressBar;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.CheckBox saveOutputCheckBox;
    }
}