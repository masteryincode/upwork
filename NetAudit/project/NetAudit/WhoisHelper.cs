﻿using System.Collections.Generic;
using System.Linq;
using NetAudit.Properties;

namespace NetAudit {
    internal class WhoisHelper {
        public static Dictionary<string, string> DomainToWhoisServer = Resources.whois.Split(
            '\n').ToDictionary(s => s.Split(' ')[0].Trim(), s => s.Split(' ')[1].Trim());
    }
}