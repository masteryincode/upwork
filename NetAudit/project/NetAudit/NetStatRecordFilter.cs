﻿using System.Linq;

namespace NetAudit {
    internal class NetStatRecordFilter : IRecordFilter<NetStatRecord> {
        public FilterTypeEnum FilterType { get; set; }
        public long[] Pids { get; set; }

        public string JoinedPids {
            get { return string.Join(", ", Pids.Select(pid => pid.ToString()).ToArray()); }
        }

        public bool Apply(string recordString, IRecordParser<NetStatRecord> parser) {
            var record = parser.Parse(recordString);

            switch (FilterType) {
                case FilterTypeEnum.ByPid:
                    if (record.RecordType.Equals(NetStatRecord.RecordTypeEnum.WithPid)) {
                        return Pids.Contains(record.Pid);
                    }

                    break;

                default:
                    return true;
            }

            return true;
        }

        public FilterTypeEnum GeType() {
            return FilterType;
        }
    }
}