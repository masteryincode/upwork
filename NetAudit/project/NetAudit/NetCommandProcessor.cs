﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using NLog;

namespace NetAudit {
    internal class NetCommandProcessor {
        private const int Timeout = 30000;
        private const int SleepAmount = 100;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private static Process _currentProcess;

        private static Process CreateProcess(string fileName, string arguments) {
            StopCurrentProcess();

            var proc = new Process {
                StartInfo = {
                    FileName = fileName,
                    Arguments = arguments,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    UseShellExecute = false
                },
                EnableRaisingEvents = true
            };

            if (CultureInfo.CurrentCulture.Equals(CultureInfo.CreateSpecificCulture("ru-RU"))) {
                proc.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(866);
            }

            _currentProcess = proc;

            return proc;
        }

        public static void StopCurrentProcess() {
            if (_currentProcess == null) {
                return;
            }

            try {
                if (_currentProcess.HasExited) {
                    _currentProcess.Refresh();
                }
                else {
                    _currentProcess.Kill();
                    _currentProcess.Refresh();
                }
            }
            catch (Exception e) {
                Log.Error(e.Message);
            }

            _currentProcess = null;
        }

        private static void SetActions(Process proc, IObserver<string> subscribe,
            Action<object, DataReceivedEventArgs> onData, Action<object, EventArgs> onExit) {
            proc.ErrorDataReceived += (sender, args) => {
                if (args.Data == null) {
                    return;
                }

                onData.Invoke(sender, args);
            };

            proc.OutputDataReceived += (sender, args) => {
                if (args.Data == null) {
                    return;
                }

                onData.Invoke(sender, args);
            };

            proc.Exited += (sender, args) => {
                subscribe.OnCompleted();

                onExit.Invoke(sender, args);

                Log.Debug(@"{0} is stopped", proc.Id);
            };
        }

        private static void Start(Process proc, Func<bool> isDone) {
            if (!proc.Start()) {
                return;
            }

            Log.Debug(@"{0} is started, current TID = {1}", proc.Id, Thread.CurrentThread.ManagedThreadId);

            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            var elapsedTime = 0;

            while (!isDone.Invoke()) {
                elapsedTime += SleepAmount;

                if (elapsedTime > Timeout) {
                    break;
                }

                Thread.Sleep(SleepAmount);
            }
        }

        public IObservable<string> ProcessNetStat(NetCommand command, NetStatRecordFilter filter) {
            var parser = new NetStatRecordParser();

            if (!command.Executable.Equals("netstat")) {
                return Observable.Empty<string>();
            }

            return Observable.Create<string>(subscribe => {
                var done = false;

                var proc = CreateProcess(command.Executable, command.JoinedArgs);

                if (filter != null && filter.GeType().Equals(FilterTypeEnum.ByPid)) {
                    subscribe.OnNext(
                        $"{command.Executable} {command.JoinedArgs} PIDs: {filter.JoinedPids}");
                }
                else {
                    subscribe.OnNext($"{command.Executable} {command.JoinedArgs}");
                }

                SetActions(proc, subscribe, (o, args) => {
                    if (filter != null && filter.Apply(args.Data, parser)) {
                        subscribe.OnNext(args.Data);
                    }
                }, (o, args) => { done = true; });

                Start(proc, () => done);

                return Disposable.Empty;
            });
        }

        public IObservable<string> Process(NetCommand command) {
            if (command.Executable.Equals("whois")) {
                return Observable.Create<string>(subscribe => {
                    subscribe.OnNext($"{command.Executable} {command.JoinedArgs}");

                    try {
                        var host = Dns.GetHostEntry(command.Args. /*Skip(1).*/First()).HostName;

                        Log.Debug("Host {0}", host);

                        var splittedHost = host.Split('.');
                        var whoisServer = "";

                        if (splittedHost.Length > 2) {
                            var domain =
                                $"{splittedHost[splittedHost.Length - 2]}{splittedHost[splittedHost.Length - 1]}";

                            if (WhoisHelper.DomainToWhoisServer.ContainsKey(domain)) {
                                whoisServer = WhoisHelper.DomainToWhoisServer[domain];
                            }
                        }

                        if (whoisServer.Length == 0 &&
                            WhoisHelper.DomainToWhoisServer.ContainsKey(splittedHost[splittedHost.Length - 1])) {
                            whoisServer = WhoisHelper.DomainToWhoisServer[splittedHost[splittedHost.Length - 1]];
                        }

                        Log.Debug("WhoisServer {0}", whoisServer);

                        if (whoisServer.Length == 0) {
                            subscribe.OnCompleted();

                            return Disposable.Empty;
                        }

                        var whoisClient = new TcpClient(whoisServer, 43);

                        var whoisStream = whoisClient.GetStream();
                        var bufferedWhoisStream = new BufferedStream(whoisStream);

                        var writer = new StreamWriter(bufferedWhoisStream);

                        writer.WriteLine(host);
                        writer.Flush();

                        var reader = new StreamReader(bufferedWhoisStream);
                        string response;

                        while ((response = reader.ReadLine()) != null) {
                            subscribe.OnNext(response);
                        }

                        whoisClient.Close();

                        return Disposable.Empty;
                    }
                    catch (Exception e) {
                        subscribe.OnError(e);
                    }
                    finally {
                        subscribe.OnCompleted();
                    }

                    return Disposable.Empty;
                });
            }

            return Observable.Create<string>(subscribe => {
                var done = false;
                var proc = CreateProcess(command.Executable, command.JoinedArgs);

                subscribe.OnNext($"{command.Executable} {command.JoinedArgs}");

                SetActions(proc, subscribe, (o, args) => { subscribe.OnNext(args.Data); }, (o, args) => { done = true; });

                Start(proc, () => done);

                return Disposable.Empty;
            });
        }
    }
}