﻿namespace NetAudit {
    internal enum FilterTypeEnum {
        ByPid
    }

    internal interface IRecordFilter<in TRecord> {
        bool Apply(string recordString, IRecordParser<TRecord> parser);

        FilterTypeEnum GeType();
    }
}