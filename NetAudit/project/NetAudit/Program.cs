﻿using System;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace NetAudit {
    internal static class Program {
        private static void WarmingUp() {
            const int sleepAmount = 300;
            var warmingUpProcessor = new NetCommandProcessor();
            var done = false;

            warmingUpProcessor.Process(NetCommand.PingBuilder.AddArgs("").Build())
                .SubscribeOn(Scheduler.ThreadPool).Subscribe(
                    next => { },
                    () => { done = true; });

            while (!done) {
                Thread.Sleep(sleepAmount);
            }
        }

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main() {
            Directory.CreateDirectory("logs");

            var loggingConfig = new LoggingConfiguration();
            var consoleTarget = new ColoredConsoleTarget();

            loggingConfig.AddTarget("console", consoleTarget);
            consoleTarget.Layout = @"${date:format=HH\:mm\:ss} ${logger} ${message}";

#if DEBUG
            var loggingRule = new LoggingRule("*", LogLevel.Debug, consoleTarget);
#else
            var loggingRule = new LoggingRule("*", LogLevel.Info, consoleTarget);
#endif

            loggingConfig.LoggingRules.Add(loggingRule);
            LogManager.Configuration = loggingConfig;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var form = new MainForm();
            var splash = new SplashScreen("img/splashscreen.jpg");

            splash.Show(true);
            WarmingUp();
            splash.Close(TimeSpan.FromMilliseconds(2000));
            Thread.Sleep(1500);
            Application.Run(form);
        }
    }
}