﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.NetworkInformation;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using NetAudit.Properties;
using NLog;

namespace NetAudit {
    public partial class MainForm : Form {
        public enum ApplicationState {
            TaskStopped,
            TaskStarted,
            TaskStopping
        }

        private readonly Dictionary<ApplicationState, HashSet<ApplicationState>> _transitions = new Dictionary<ApplicationState, HashSet<ApplicationState>> {
            {ApplicationState.TaskStopped, new HashSet<ApplicationState> {ApplicationState.TaskStarted, ApplicationState.TaskStopped} },
            {ApplicationState.TaskStarted, new HashSet<ApplicationState> { ApplicationState.TaskStopped, ApplicationState.TaskStopping} },
            {ApplicationState.TaskStopping, new HashSet<ApplicationState> {ApplicationState.TaskStopped } }
        };

        private readonly Dictionary<ApplicationState, Action> _onStateActions;

        private ApplicationState _currentState = ApplicationState.TaskStopped;

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private static readonly TimeSpan RefreshInterval = TimeSpan.FromSeconds(3);

        private readonly HashSet<ProcessInfo> _allProcesses =
            new HashSet<ProcessInfo>(ProcessInfoEqualityComparer.Instance);

        private readonly HashSet<ProcessInfo> _filteredProcesses =
            new HashSet<ProcessInfo>(ProcessInfoEqualityComparer.Instance);

        private string _filter = "";
        private long _bytesReceived;
        private long _bytesSent;
        private long _currentSpeed;
        private long _usage; // per mille (1/1000)

        private readonly NetCommandProcessor _processor = new NetCommandProcessor();

        public MainForm() {
            InitializeComponent();

            processSearchTextBox.TextChanged += (sender, args) => {
                _filter = processSearchTextBox.Text;

                RefreshProcesses();
            };

            DoPeriodicWork();

            Observable.Interval(RefreshInterval)
                .SubscribeOn(Scheduler.ThreadPool)
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(_ => DoPeriodicWork());

            _onStateActions = new Dictionary<ApplicationState, Action> {
            {ApplicationState.TaskStarted, () => {
                pingButton.Enabled = false;
                tracerouteButton.Enabled = false;
                whoisButton.Enabled = false;
                inspectTrafficButton.Enabled = false;

                stopButton.Enabled = true;
            } },
            {ApplicationState.TaskStopping, () => {
                pingButton.Enabled = false;
                tracerouteButton.Enabled = false;
                whoisButton.Enabled = false;
                inspectTrafficButton.Enabled = false;

                stopButton.Enabled = false;
            } },
            {ApplicationState.TaskStopped, () => {
                pingButton.Enabled = true;
                tracerouteButton.Enabled = true;
                whoisButton.Enabled = true;
                inspectTrafficButton.Enabled = true;

                stopButton.Enabled = false;
            } }};

            stopButton.Click += (sender, args) => {
                ChangeState(ApplicationState.TaskStopping);
                StopCurrentProcess();
            };

            pingButton.Click += (sender, args) => {
                ProcessPing(urlTextInput.Text);
            };

            tracerouteButton.Click += (sender, args) => {
                ProcessTraceroute(urlTextInput.Text);
            };

            whoisButton.Click += (sender, args) => {
                ProcessWhois(urlTextInput.Text);
            };

            inspectTrafficButton.Click += (sender, args) => {
                ProcessInspectTraffic();
            };

            processesListView.DoubleClick += (sender, args) => {
                if (!_currentState.Equals(ApplicationState.TaskStopped))
                {
                    return;
                }

                ProcessNetStat(long.Parse(processesListView.FocusedItem.Text));
            };

            linkLabel.Click += (sender, args) => {
                Process.Start(Resources.Website);
            };

            ChangeState(ApplicationState.TaskStopped);
        }

        private void ChangeState(ApplicationState newState) {
            if (!_transitions[_currentState].Contains(newState)) {
                return;
            }

            _currentState = newState;
            _onStateActions[newState].Invoke();
        }

        private void CollectProcesses() {
            var newProcesses = new HashSet<ProcessInfo>(ProcessInfoEqualityComparer.Instance);

            Observable.Create<ProcessInfo>(observer => {
                Process.GetProcesses()
                    .ToList()
                    .ForEach(process => observer.OnNext(new ProcessInfo {Id = process.Id, Name = process.ProcessName}));
                observer.OnCompleted();

                return Disposable.Empty;
            }).SubscribeOn(Scheduler.ThreadPool).ObserveOn(SynchronizationContext.Current).Subscribe(
                info => newProcesses.Add(info),
                () => {
                    var toAdd =
                        new HashSet<ProcessInfo>(
                            newProcesses.Except(_allProcesses, ProcessInfoEqualityComparer.Instance),
                            ProcessInfoEqualityComparer.Instance);
                    var toRemove =
                        new HashSet<ProcessInfo>(
                            _allProcesses.Except(newProcesses, ProcessInfoEqualityComparer.Instance),
                            ProcessInfoEqualityComparer.Instance);

                    if (toAdd.Count == 0 && toRemove.Count == 0) {
                        return;
                    }

                    _allProcesses.UnionWith(toAdd);
                    _allProcesses.ExceptWith(toRemove);
                });
        }

        private void RefreshProcesses() {
            var newFilteredProcesses =
                new HashSet<ProcessInfo>(
                    _allProcesses.Where(p => _filter.Trim().Length == 0 || p.Name.StartsWith(_filter, true, CultureInfo.CurrentCulture)),
                    ProcessInfoEqualityComparer.Instance);

            var toAdd =
                new HashSet<ProcessInfo>(
                    newFilteredProcesses.Except(_filteredProcesses, ProcessInfoEqualityComparer.Instance),
                    ProcessInfoEqualityComparer.Instance);

            Log.Debug("Processes to add = {0}", toAdd.Count);

            var toRemove =
                new HashSet<ProcessInfo>(
                    _filteredProcesses.Except(newFilteredProcesses, ProcessInfoEqualityComparer.Instance),
                    ProcessInfoEqualityComparer.Instance);

            Log.Debug("Processes to remove = {0}", toRemove.Count);

            if (toAdd.Count == 0 && toRemove.Count == 0) {
                return;
            }

            _filteredProcesses.UnionWith(toAdd);
            _filteredProcesses.ExceptWith(toRemove);

            processesListView.BeginUpdate();
            foreach (ListViewItem i in processesListView.Items) {
                var p = new ProcessInfo {Id = int.Parse(i.SubItems[0].Text), Name = i.SubItems[1].Text};

                if (toRemove.Contains(p)) {
                    i.Remove();
                }
            }
            processesListView.EndUpdate();

            processesListView.BeginUpdate();
            toAdd.ToList().ForEach(p => {
                var item = new ListViewItem(p.Id.ToString());

                item.SubItems.Add(p.Name);

                processesListView.Items.Add(item);
            });
            processesListView.EndUpdate();
        }

        private void CollectTrafficAndUsage() {
            if (!NetworkInterface.GetIsNetworkAvailable()) {
                return;
            }

            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            long received = 0;
            long sent = 0;
            long maxSpeed = 0;

            foreach (var i in networkInterfaces.Where(@interface =>
                @interface.IsReceiveOnly == false
                && @interface.OperationalStatus == OperationalStatus.Up
                && !@interface.Description.StartsWith("Virtual")
                && @interface.NetworkInterfaceType != NetworkInterfaceType.Loopback)) {
                received += i.GetIPv4Statistics().BytesReceived;
                sent += i.GetIPv4Statistics().BytesSent;

                maxSpeed = Math.Max(i.Speed, maxSpeed);

                Log.Debug("(r:{0} s:{1} sp:{2})", i.GetIPv4Statistics().BytesReceived, i.GetIPv4Statistics().BytesSent,
                    i.Speed);
            }

            long receivedDelta = Math.Abs(received - _bytesReceived);
            long sentDelta = Math.Abs(sent - _bytesSent);

            _currentSpeed = (_bytesReceived == 0 && _bytesSent == 0)
                ? 0
                : (receivedDelta + sentDelta) * 8 / RefreshInterval.Seconds;

            _bytesReceived = received;
            _bytesSent = sent;
            _usage = (maxSpeed == 0 || _currentSpeed > maxSpeed) ? 0 : (_currentSpeed * 1000 / maxSpeed);

            Log.Debug("CurSpeed: {0}, MaxSpeed: {1}, Usage: {2}", _currentSpeed, maxSpeed, _usage);
        }

        private void UpdateTraffic() {
            sentLabel.Text = $"{_bytesSent:n0}";
            receivedLabel.Text = $"{_bytesReceived:n0}";
            currentSpeedLabel.Text = Utils.FormatSpeed(_currentSpeed);
        }

        private void UpdateUsage() {
            usageProgressBar.Value = (int) _usage;
            usageProgressBar.Refresh();
        }

        private void DoPeriodicWork() {
            CollectProcesses();
            RefreshProcesses();
            CollectTrafficAndUsage();
            UpdateTraffic();
            UpdateUsage();
        }

        private void DoCommandStuff(IObservable<string> source, string commandName) {
            ChangeState(ApplicationState.TaskStarted);
            StreamWriter outfile = null;

            if (saveOutputCheckBox.Checked) {
                outfile =
                    File.CreateText($"logs/{commandName}_{DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss.ffffff")}.txt");
            }

            stopButton.Enabled = true;
            logListBox.Items.Clear();

            workProgressBar.Style = ProgressBarStyle.Marquee;
            workProgressBar.MarqueeAnimationSpeed = 500;

            Log.Debug(@"{0}: Current TID = {1}", commandName, Thread.CurrentThread.ManagedThreadId);

            source.SubscribeOn(Scheduler.ThreadPool).ObserveOn(SynchronizationContext.Current).Subscribe(
                next => {
                    logListBox.Items.Add(next);

                    if (outfile != null && saveOutputCheckBox.Checked) {
                        outfile.WriteLine(next);
                    }
                },
                ex => { Log.Error(ex.Message); },
                () => {
                    workProgressBar.Style = ProgressBarStyle.Continuous;
                    workProgressBar.MarqueeAnimationSpeed = 0;

                    if (outfile != null && saveOutputCheckBox.Checked) {
                        outfile.Close();
                    }

                    ChangeState(ApplicationState.TaskStopped);
                });
        }

        private void ProcessPing(string urlOrIp) {
            DoCommandStuff(_processor.Process(NetCommand.PingBuilder.AddArgs(urlOrIp).Build()), "ping");
        }

        private void ProcessNetStat(long pid) {
            DoCommandStuff(
                _processor.ProcessNetStat(NetCommand.NetStatCommand,
                    new NetStatRecordFilter {FilterType = FilterTypeEnum.ByPid, Pids = new[] {pid}}), "netstat");
        }

        private void ProcessTraceroute(string urlOrIp) {
            DoCommandStuff(_processor.Process(NetCommand.TracerouteBuilder.AddArgs(urlOrIp).Build()), "traceroute");
        }

        private void ProcessInspectTraffic() {
            DoCommandStuff(_processor.Process(NetCommand.InspectTrafficCommand), "inspecttraffic");
        }

        private void ProcessWhois(string urlOrIp) {
            DoCommandStuff(_processor.Process(NetCommand.WhoisBuilder.AddArgs(urlOrIp).Build()), "whois");
        }

        private static void StopCurrentProcess() {
            NetCommandProcessor.StopCurrentProcess();
        }
    }
}