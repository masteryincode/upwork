﻿namespace NetAudit {
    internal interface IRecordParser<out TRecord> {
        TRecord Parse(string s);
    }
}