#ifndef RULES_HPP
#define RULES_HPP

//Calculates the neighbours number around a (x, y) in the array with size: (width, height)
int getNeighboursNumber(char** array, int width, int height, int x, int y);

//Applies the Game of Life rules to the visibleArray and put changes to the tempArray for current position (x, y)
//Returns 1 (true) if new cell was born or old cell survived, otherwise returns 0 (false)
int applyRules(char** visibleArray, char** tempArray, int width, int height, int x, int y);

//Applies the Game of Life rules to the visibleArray and put changes to the tempArray
//Returns current population (the number of live cells)
long applyRules(char** visibleArray, char** tempArray, int width, int height);


#endif // RULES_HPP