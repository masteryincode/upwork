#ifndef PATTERNS_HPP
#define PATTERNS_HPP

//Returns pattern width by pattern id
int getPatternWidth(int patternId);

//Returns pattern height by pattern id
int getPatternHeight(int patternId);

//Tries to put the Game of Life pattern with patternId into centered map part (a visible part) with size (visibleWidth, visibleHeight) on coordinates (xStart, yStart)
//Returns starting population (the number of live cells) if pattern was put successfully, otherwise - 0
long putPattern(char** map, int width, int height, int visibleWidth, int visibleHeight, int patternId, int xStart, int yStart);

#endif // PATTERNS_HPP