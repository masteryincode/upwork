#ifndef VISIBLEWORLD_HPP
#define VISIBLEWORLD_HPP

//Suspends execution (of current thread) for `seconds` seconds and `nanoseconds` nanoseconds
void lifeSleep(unsigned seconds, unsigned long nanoseconds);

//Clears the current terminal screen
void clearScreen();

//Draws a line with `width` width filled by c chars
void drawLine(int width, char c);

//Draws current world visible part (centered)
void drawWorld(char** map, int width, int height, int visibleWidth, int visibleHeight);

#endif // VISIBLEWORLD_HPP