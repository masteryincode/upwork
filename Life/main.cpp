#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "rules.hpp"
#include "patterns.hpp"
#include "visibleworld.hpp"

void lifeSleep(unsigned seconds, unsigned long nanoseconds) {
    timespec tim, tim2;
    
    tim.tv_sec = seconds;
    tim.tv_nsec = nanoseconds;
    
    nanosleep(&tim, &tim2); //usleep is obsolete. Use nanosleep.
}

// Allocating dynamic (on heap) memory for the map's array (in some cases map's array may not allocated on the stack)
char** createfilledArray(int width, int height, char c) {
    //allocating memory for 2D array
    char** result = (char**)malloc(height * sizeof(char*));
    
    if (result == NULL) {
        printf("Can't create array: not enougth memory");
        exit(1);
    }
    
    for (int y = 0; y < height; y++) {
        //allocating memory for each sub arrays
        result[y] = (char*)malloc(width * sizeof(char));
        
        if (result[y] == NULL) {
            printf("Can't create array: not enougth memory");
            exit(1);
        }
        
        // filling the array
        for (int x = 0; x < width; x++) {
            result[y][x] = c;
        }
    }
    
    return result;
}

// Freeing the allocated memory for the map's array
void destroyArray(char** array, int height) {
    for (int y = 0; y < height; y++) {
        free(array[y]);
    }
    
    free(array);
}

int main() {
    //Just the size constants, not global variables
    #define FULL_WIDTH 60
    #define FULL_HEIGHT 40
    #define WIDTH 40
    #define HEIGHT 20

    //Showing info
    clearScreen();
    
    printf("\tGAME OF LIFE\n\n\
The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.\n\n\
The 'game' is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. \nOne interacts with the Game of \
Life by creating an initial configuration and observing how it evolves or, for advanced players, by creating patterns with particular properties.\n\n\
\tRULES\n\n\
- Any live cell with fewer than two live neighbours dies, as if caused by under-population.\n\
- Any live cell with two or three live neighbours lives on to the next generation.\n\
- Any live cell with more than three live neighbours dies, as if by over-population.\n\
- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.\n\n");
    printf("Press [Enter] to continue");
    getchar(); // wait till user press [Enter]
    clearScreen(); //Clearing the screen
    
    //Creating two arrays (temp & visible) dynamically
    char** arrays[2] = {createfilledArray(FULL_WIDTH, FULL_HEIGHT, ' '), createfilledArray(FULL_WIDTH, FULL_HEIGHT, ' ')};
    
    // current array index
    int currentArray = 0;
    
    // starting coordinates
    int xStart = 0, yStart = 0;
    
    // flag which shows then all is ok =)
    int ok = 1;
    
    do {
        //Asking user to enter starting coordinates
        printf("Enter the X start coordinate (0 .. %d): ", WIDTH - 1); scanf("%d", &xStart);
        printf("Enter the Y start coordinate (0 .. %d): ", HEIGHT - 1); scanf("%d", &yStart);
        
        //Checking the coordinates for entering in size (WIDTH, HEIGHT)
        ok = xStart >= 0 && xStart < WIDTH && yStart >= 0 && yStart < HEIGHT;
        
        clearScreen();
    } while (!ok); //repeat asking if the user entered wrong coordinates
    
    int patternId = 0;
    
    do {
        //Asking user to select pattern 
        printf("Select pattern (1 .. 3):\n");
        printf("  1) Glider\n");
        printf("  2) Oscillator\n");
        printf("  3) Glider Gun\n");
        printf("> "); scanf("%d", &patternId);
        
        //Checking selected pattern
        ok = patternId >= 1 && patternId <= 3;
        
        clearScreen();
    } while (!ok); //repeat asking if the user selected a wrong pattern 
    
    int numberOfGenerations = 0;
    
    do {
        //Asking user to enter the number of generations
        printf("Enter the number of generations (>0): "); scanf("%d", &numberOfGenerations);
        
        ok = numberOfGenerations > 0;
        
        clearScreen();
    } while (!ok);
    
    //trying to put pattern into current map
    long population = putPattern(arrays[currentArray], FULL_WIDTH, FULL_HEIGHT, WIDTH, HEIGHT, patternId, xStart, yStart);
    
    //if can't put show error and free memory
    if (population == 0) {
        printf("\nThe initial generation is out of map\n");
        
        // freeing memory
        destroyArray(arrays[0], FULL_HEIGHT);
        destroyArray(arrays[1], FULL_HEIGHT);
        
        return 1;
    }
    
    for (int generation = 0; generation < numberOfGenerations && population > 0; generation++) {
        drawWorld(arrays[currentArray], FULL_WIDTH, FULL_HEIGHT, WIDTH, HEIGHT); //drawing the game of life world
        population = applyRules(arrays[currentArray], arrays[!currentArray], FULL_WIDTH, FULL_HEIGHT); //applying the game of life rules and calculating a population
        
        currentArray = !currentArray; //array swapping
        lifeSleep(0, 125000000); // sleep 0.125 sec (~ 8 fps)
    }
    
    printf("Press [Enter] to continue\n");
    getchar(); // wait till user press [Enter]
    
    // freeing memory
    destroyArray(arrays[0], FULL_HEIGHT);
    destroyArray(arrays[1], FULL_HEIGHT);
    
    return 0;
}