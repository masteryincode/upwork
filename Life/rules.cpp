#include "rules.hpp"

int getNeighboursNumber(char** array, int width, int height, int x, int y) {
    int neighbours = 0;
    
    // counting all neighbours
    if (x == 0) {
        if (y == 0) { //The current cell is in the top left map's corner: checking only 3 neighbours
            if (array[y][x + 1] == '*') {       //
                neighbours++;                                  //  o ?
            }                                                  //  ? ?
            if (array[y + 1][x + 1] == '*') {   //
                neighbours++;
            }
            if (array[y + 1][x] == '*') {
                neighbours++;
            }
        } else if (y == height - 1) { //The current cell is in the bottom left map's corner: checking only 3 neighbours
            if (array[y - 1][x] == '*') {       //
                neighbours++;                                  //  ? ?
            }                                                  //  o ?
            if (array[y - 1][x + 1] == '*') {   //
                neighbours++;
            }
            if (array[y][x + 1] == '*') {
                neighbours++;
            }
        } else { //The current cell is on the left maps' line: checking only 5 neighbours
            if (array[y - 1][x] == '*') {       //
                neighbours++;                                  //  ? ?
            }                                                  //  o ?
            if (array[y - 1][x + 1] == '*') {   //  ? ?
                neighbours++;                                  //
            }
            if (array[y][x + 1] == '*') {
                neighbours++;
            }
            if (array[y + 1][x + 1] == '*') {
                neighbours++;
            }
            if (array[y + 1][x] == '*') {
                neighbours++;
            }
        }
    } else if (x == width - 1) {
        if (y == 0) { //The current cell is int the top right map's corner: checking only 3 neighbours
            if (array[y][x - 1] == '*') {       //
                neighbours++;                                  //  ? o
            }                                                  //  ? ?
            if (array[y + 1][x - 1] == '*') {   //
                neighbours++;
            }
            if (array[y + 1][x] == '*') {
                neighbours++;
            }
        } else if (y == height - 1) { //The current cell is in the bottom right map's corner: checking only 3 neighbours
            if (array[y][x - 1] == '*') {       //
                neighbours++;                                  //  ? ?
            }                                                  //  ? o
            if (array[y - 1][x - 1] == '*') {   //
                neighbours++;
            }
            if (array[y - 1][x] == '*') {
                neighbours++;
            }
        } else { //The current cell is on the right map's line: checking only 5 neighbours
            if (array[y + 1][x] == '*') {       //
                neighbours++;                                  //  ? ?
            }                                                  //  ? o
            if (array[y + 1][x - 1] == '*') {   //  ? ?
                neighbours++;                                  //
            }
            if (array[y][x - 1] == '*') {
                neighbours++;
            }
            if (array[y - 1][x - 1] == '*') {
                neighbours++;
            }
            if (array[y - 1][x] == '*') {
                neighbours++;
            }
        }
    } else {
        if (y == 0) { //The current cell is on the top map's line: checking only 5 neighbours
            if (array[y][x - 1] == '*') {       //
                neighbours++;                                  //  ? o ?
            }                                                  //  ? ? ?
            if (array[y + 1][x - 1] == '*') {   //
                neighbours++;
            }
            if (array[y + 1][x] == '*') {
                neighbours++;
            }
            if (array[y + 1][x + 1] == '*') {
                neighbours++;
            }
            if (array[y][x + 1] == '*') {
                neighbours++;
            }
        } else if (y == height - 1) { //The current cell is on the bottom map's line: checking only 5 neighbours
            if (array[y][x - 1] == '*') {       //
                neighbours++;                                  //  ? ? ?
            }                                                  //  ? o ?
            if (array[y - 1][x - 1] == '*') {   //
                neighbours++;
            }
            if (array[y - 1][x] == '*') {
                neighbours++;
            }
            if (array[y - 1][x + 1] == '*') {
                neighbours++;
            }
            if (array[y][x + 1] == '*') {
                neighbours++;
            }
        } else { //Else: check all 8 neighbours
            if (array[y - 1][x - 1] == '*') {   //
                neighbours++;                                  //  ? ? ?
            }                                                  //  ? o ?
            if (array[y - 1][x] == '*') {       //  ? ? ?
                neighbours++;                                  //
            }
            if (array[y - 1][x + 1] == '*') {
                neighbours++;
            }
            if (array[y][x + 1] == '*') {
                neighbours++;
            }
            if (array[y + 1][x + 1] == '*') {
                neighbours++;
            }
            if (array[y + 1][x] == '*') {
                neighbours++;
            }
            if (array[y + 1][x - 1] == '*') {
                neighbours++;
            }
            if (array[y][x - 1] == '*') {
                neighbours++;
            }
        }
    }
    
    /* alternate version
    for (int j = y - 1; j <= y + 1; j++) {
        for (int i = x - 1; i <= x + 1; i++) {
            if (i >= 0 && j >=0 && i < width && j < height && !(i == x && j == y)) { //iterates over all possible neighbours but checks bounds and (i, j) != (x, y)
                if (array[j][i] == '*') {
                    neighbours++;
                }
            }
        }
    }
    */
    
    return neighbours;
}

int applyRules(char** visibleArray, char** tempArray, int width, int height, int x, int y) {
    int neighbours = getNeighboursNumber(visibleArray, width, height, x, y);
    
    if ((visibleArray[y][x] == ' ' && neighbours == 3) || (visibleArray[y][x] == '*' && (neighbours == 2 || neighbours == 3))) { 
        tempArray[y][x] = '*';
        
        return 1;
    } else {
        tempArray[y][x] = ' ';
        
        return 0;
    }
}

long applyRules (char** visibleArray, char** tempArray, int width, int height) {
    long population = 0;
    
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {          
            population += applyRules(visibleArray, tempArray, width, height, x, y);
        }
    }
    
    return population;
}



