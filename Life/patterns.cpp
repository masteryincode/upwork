#include "patterns.hpp"

int getGliderWidth() {
    return 3;
}

int getGliderHeight() {
    return 3;
}

int getOscillatorWidth() {
    return 3;
}

int getOscillatorHeight() {
    return 1;
}

int getGliderGunWidth() {
    return 36;
}

int getGliderGunHeight() {
    return 9;
}


int getPatternWidth (int patternId) {
    switch (patternId) {
        case 1: return getGliderWidth();
        case 2: return getOscillatorWidth();
        case 3: return getGliderGunWidth();
    }
    
    return -1;
}

int getPatternHeight (int patternId) {
    switch (patternId) {
        case 1: return getGliderHeight();
        case 2: return getOscillatorHeight();
        case 3: return getGliderGunHeight();
    }
    
    return -1;
}

long putPattern(char** map, int width, int height, int visibleWidth, int visibleHeight, int patternId, int xStart, int yStart) {
    //Checking pattern size
    int patternHeight = getPatternHeight(patternId);
    int patternWidth = getPatternWidth(patternId);
    
    //calculating a visible part offset
    int dx = (width - visibleHeight) / 2;
    int dy = (height - visibleHeight) / 2;

    if ((dx + xStart + getPatternWidth(patternId)) >= width || (dy + yStart + getPatternHeight(patternId)) >= height) {
        return 0; //can't fit pattern into map
    }
    
    char glider[3][3] = {
        {' ', ' ', '*'},
        {'*', ' ', '*'},
        {' ', '*', '*'}};
    
    char oscillator[1][3] = {{'*', '*', '*'}};
    
    char gliderGun[9][36] = {
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', '*', ' ', ' ', ' ', ' ', ' ', ' ', '*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', '*'},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', '*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', '*'},
        {'*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', '*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {'*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', '*', ' ', '*', '*', ' ', ' ', ' ', ' ', '*', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', ' ', ' ', ' ', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
        {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*', '*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '}};
    
    long population = 0;    
        
    //Placing pattern into current map
    for (int y = 0; y < patternHeight; y++) {
        for (int x = 0; x < patternWidth; x++) {
            //calculating a cell position
            int xPos = dx + xStart + x;
            int yPos = dy + yStart + y;
            
            switch (patternId) {
                case 1: map[yPos][xPos] = glider[y][x]; break;
                case 2: map[yPos][xPos] = oscillator[y][x]; break;
                case 3: map[yPos][xPos] = gliderGun[y][x]; break;
            }
            
            //Calculating current population
            if (map[yPos][xPos] == '*') {
                population++;
            }
        }
    }
    
    return population;
}