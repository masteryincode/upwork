#include <stdlib.h>
#include <stdio.h>
#include "visibleworld.hpp"

void clearScreen() {
    system("clear"); //Linux hasn't `cls` command.
}

void drawLine(int width, char c) {
    for (int x = 0; x < width; x++) {
        putchar(c);
    }
}

void drawWorld (char** map, int width, int height, int visibleWidth, int visibleHeight) {
    clearScreen();
    drawLine(visibleWidth, '-'); putchar('+'); putchar('\n');
    
    //calculating a visible part offset
    int dx = (width - visibleHeight) / 2;
    int dy = (height - visibleHeight) / 2;
    
    for (int y = 0; y < visibleHeight; y++) {
        for (int x = 0; x < visibleWidth; x++) {
            //Drawing the current array cells
            putchar(map[dy + y][dx + x]);
        }
        
        putchar('|'); putchar('\n');
    }
    
    drawLine(visibleWidth, '-'); putchar('+'); putchar('\n');
}