﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Brushes = System.Drawing.Brushes;

namespace TerminalApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window

    {
        private int queueNum;
        private Font printFont;
        private StreamReader streamToPrint;


        public MainWindow()
        {
            InitializeComponent();
            tbQueue.FontSize = 72;
            tbQueue.TextAlignment = TextAlignment.Center;

            tbQueue.Text = queueNum.ToString();
        }

        private void richTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            queueNum++;
            tbQueue.Text = queueNum.ToString();
            doWork(queueNum);
            

        }

        private void createTicket(int n)
        {
            string str = "Welcome \n Your ticket number is: " + n;
            string path = String.Format(@".\{0}.txt", n);
            System.IO.File.WriteAllText(path,str);
        }

        private void printTicket(int s)
        {
            string path = String.Format(@".\{0}.txt", s);
            try
            {
                streamToPrint = new StreamReader (path);
                try
                {
                    printFont = new Font("Arial", 10);
                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler
                       (this.pd_PrintPage);
                    pd.Print();
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void push2Server()
        {

        }
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }
        // Task factory

        private void doWork(int s)
        {
            try
            {
                createTicket(s);
            }
            catch (Exception)
            {

                throw;
            }

            try
            {
                push2Server();
            }
            catch (Exception)
            {

                throw;
            }

            try
            {
                printTicket(s);
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
