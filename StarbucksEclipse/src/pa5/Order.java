package pa5;

public class Order {
    private final Integer arrivalTime;
    private final String customerName;
    private final Integer complexity;
    /*
	 * Name: order
	 * Purpose: makes an object given the values sent to it
	 * Parameter: time, customerName, complexity
	 * Return: n/a
	 */
    public Order(Integer arrivalTime, String customerName, Integer complexity) { //Order class constructor
        this.arrivalTime = arrivalTime;
        this.customerName = customerName;
        this.complexity = complexity;
    }
    /*
	 * Name: getArrivalTime()
	 * Purpose: returns Arrival time
	 * Parameter: n/a
	 * Return: Integer
	 */
    public Integer getArrivalTime() {
        return arrivalTime;
    }
    /*
	 * Name: getCustomerName
	 * Purpose: returns customers name
	 * Parameter: n/a
	 * Return: String
	 */
    public String getCustomerName() {
        return customerName;
    }
    /*
	 * Name: getComplexity
	 * Purpose: returns the complexity of the drink
	 * Parameter: n/a
	 * Return: Integer
	 */
    public Integer getComplexity() {
        return complexity;
    }

    
    /*
   	 * Name: fromString
   	 * Purpose: Try to parse string (line from file) to (Integer time, 
   	 * String customerName, Integer complexity) and store it to Order fields
   	 * Parameter: n/a
   	 * Return: Integer
   	 */
    /// Try to parse string (line from file) to (Integer time, String customerName, Integer complexity) and store it to Order fields
    /// Returns order record or null
    public static Order fromString(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }

        /// Split line parts which separated by commas or spaces.
        String[] parts = s.split(",\\s*|\\s+");

        if (parts.length != 3) {
            return null;
        }

        try {
            Integer arrivalTime = Integer.parseInt(parts[0]);
            String customerName = parts[1];
            Integer complexity = Integer.parseInt(parts[2]);

            return new Order(arrivalTime, customerName, complexity);
        } catch (NumberFormatException ignored) {
            return null;
        }
    }
}
