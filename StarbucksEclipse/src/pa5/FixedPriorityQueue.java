package pa5;
/*
More information is provided in web links attached to covering letter
 */


/// A bounded Min Priority Queue based on Binary Heap
public class FixedPriorityQueue {
    private Order[] orders;
    private int size = 0;
    private int maxSize = 10;
    /*
   	 * Name: FixedPriorityQueueOfOrders
   	 * Purpose: class constructor
   	 * Parameter: int
   	 * Return: n/a
   	 */
    public FixedPriorityQueue(int maxSize) {
        this.maxSize = maxSize;

        // +1 element to simplify indexing and binary search
        this.orders = new Order[maxSize + 1];
    }
    /*
	 * Name: getSize
	 * Purpose: takes int size
	 * Parameter: n/a
	 * Return: int size
	 */
    public int getSize() {
        return size;
    }
    /*
   	 * Name: getMaxSize
   	 * Purpose: returns max size
   	 * Parameter: n/a
   	 * Return: int
   	 */
    public int getMaxSize() {
        return maxSize;
    }
    /*
	 * Name: isEmpty
	 * Purpose: checks to see if empty
	 * Parameter: n/a
	 * Return: boolean
	 */
    public boolean isEmpty() {
        return size == 0;
    }
    /*
	 * Name: add
	 * Purpose: Inserts the specified element in the queue if it is possible. Returns true if it is
	 * Parameter: element
	 * Return: boolean
	 */
    /// Inserts the specified element into this queue if it is possible. Returns true on success
    public boolean add(Order element) {
        if (size == maxSize) {
            return false;
        } else {
            size++;
            orders[size] = element;

            int key = size;
            
            // binary search and "insert"
            while (key > 1) {
                int newKey = key / 2;

                if (orders[newKey].getArrivalTime() > orders[key].getArrivalTime()) {
                	// temporary element helps to exchange elements
                    Order temp = orders[newKey];
                    
                    orders[newKey] = orders[key];
                    orders[key] = temp;
                    key = newKey;
                } else {
                    break;
                }
            }

            return true;
        }
    }

    /// Retrieves and removes the head (Order) of this queue, or returns null if this queue is empty.
	public Order poll() {
        if (isEmpty()) {
            return null;
        }

        // interest element has index = 1 (always), but we should help to binary search, so exchange first and last elements
        Order temp = orders[1]; //temporary element helps to exchange elements
        
        orders[1] = orders[size];
        orders[size] = temp;
        
        // take interest element
        Order result = orders[size];

        // decrease size
        size--;

        /// first element index;
        int key = 1;
        
        // "infinite" loop: process reverse binary search to rebuild binary heap in array 
        while(true) {
            int newKey = key * 2;

            if (newKey <= size) {
                if (newKey < size && orders[newKey].getArrivalTime() > orders[newKey + 1].getArrivalTime()) {
                    newKey++;
                }

                if (orders[key].getArrivalTime() <= orders[newKey].getArrivalTime()) {
                    break;
                }
                //temporary element helps to exchange elements
                Order tempOrder = orders[newKey];
                
                orders[newKey] = orders[key];
                orders[key] = tempOrder;

                key = newKey;
            } else {
                break;
            }
        }

        orders[size + 1] = null;

        return result;
    }
}
