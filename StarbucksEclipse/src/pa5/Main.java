package pa5;

import java.io.*;
import java.util.Scanner;

public class Main {
    private static final Integer ORDER_FILL_MULTIPLIER = 2;
    private static final Integer QUEUE_SIZE = 2;
    
    final static FixedPriorityQueue queue = new FixedPriorityQueue(QUEUE_SIZE);
    /*
   	 * Name: main
   	 * Purpose: main method
   	 * Parameter: args
   	 * Return: void
   	 */
    public static void main(String[] args) {
        String fileName;
        
        /// Trying to take filename from the first argument of command line or read it from console
        if (args.length == 1) {
            fileName = args[0];
        } else {
            System.out.print("Enter file name: ");
            Scanner in = new Scanner(System.in);
            
            try {
            	fileName = in.nextLine();
            } finally {
            	in.close();
            }
        }

        File file = new File(fileName);

        if (!file.exists()) {
            System.err.println("File doesn't exists");

            return;
        }

        if (!file.isFile()) {
            System.err.println("It is not a file");
        }

        BufferedReader reader = null;
        
        try {
        	reader = new BufferedReader(new FileReader(file));
        	
            fillQueue(reader);

            doWork(reader);
        } catch (FileNotFoundException ignored) {
        } catch (IOException e) {
            System.err.println("Can't read the file");
        } finally {
        	if (reader != null) {
        		try {
        			reader.close();
        		} catch (IOException ignored) {
        			
        		}
        	}
        }
    }
    /*
	 * Name: fillQueue
	 * Purpose: fills the queue with data from the file
	 * Parameter: queue, reader
	 * Return: void
	 */
    /// Fills queue
    static void fillQueue(final BufferedReader reader) throws IOException {
        for (int i = 0; i < QUEUE_SIZE; i++) {
            if (!arrive(reader)) {
                break;
            }
        }
    }
    /*
	 * Name: arrive
	 * Purpose: /// Read line from file (buffered reader provides reading line by line), 
	 * parse it to an order and try to put it to queue. Returns true if success.
	 * Parameter: queue, reader
	 * Return: boolean
	 */
    /// Read line from file (buffered reader provides reading line by line), parse it to an order and try to put it to queue. Returns true if success.
    static boolean arrive(final BufferedReader reader) throws IOException {
        final String line = reader.readLine();

        if (line == null) {
            return false;
        } else if (line.isEmpty()) {
            return true;
        }

        final Order order = Order.fromString(line);

        if (order == null) {
            System.err.printf("Wrong format: '%s'\n", line);

            return true;
        }

        queue.add(order);
        System.out.println(order.getCustomerName() + " enters Starbucks at " + order.getArrivalTime());

        return true;
    }
    /*
  	 * Name: doWork
  	 * Purpose: does the analysis of the queue, (This method does the main work: takes orders from queue, 
  	 * process it, calculates waiting time etc)
  	 * Parameter: n/a
  	 * Return: Integer
  	 */
    /// This is method that do the main work: takes order from queue, process it, calculate waiting time etc
    static void doWork(final BufferedReader reader) throws IOException {
        boolean isFirst = true;
        Integer currentTime = 0;
        Long sumWaitingTime = 0l;
        Long ordersNumber = 0l;

        while (true) {
            final Order order = queue.poll();

            // Queue is empty?
            if (order == null) {
                if (arrive(reader)) {
                    continue;
                }

                break;
            }

            final Integer helpingTime = order.getComplexity() * ORDER_FILL_MULTIPLIER;

            if (isFirst) {
                currentTime = order.getArrivalTime();
                isFirst = false;
            }

            currentTime = currentTime + helpingTime;

            final Integer waitingTime = currentTime - order.getArrivalTime();

            System.out.println(order.getCustomerName() + " is helped by Li in " + helpingTime + ". Waiting time = " + waitingTime);
            System.out.println(order.getCustomerName() + " left StarBucks at " + currentTime);

            ordersNumber++;
            sumWaitingTime = sumWaitingTime + waitingTime.longValue();
            arrive(reader);
        }

        Long averageWaitingTime = 0l;
        
        if (ordersNumber > 0) {
        	averageWaitingTime = sumWaitingTime / ordersNumber;
        }

        System.out.println("The average waiting time is " + averageWaitingTime);
    }

     
}
