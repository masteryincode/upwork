﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace SoftInstallerDeinstaller.Tasks {
    public abstract class MyCommonTask: IMyTask {
        private const int DefaultTimeout = 10*60*1000; //ms
        private const int SleepAmount = 100; //ms

        public string Name { get; set; }

        public int Priority => (int)TaskType;

        public abstract TaskType TaskType { get; }

        public abstract string FileName { get; set; }

        public abstract string Arguments { get; set; }

        public int Timeout { get; set; } //sec

        public Task<TaskResult> Create() {
            return new Task<TaskResult>(() => {
                var done = false;
                var currentTimeout = Timeout == 0 ? DefaultTimeout : (Timeout * 1000);

                var p = new Process
                {
                    StartInfo = {
                        FileName = FileName,
                        Arguments = Arguments,
                        CreateNoWindow = true,
                        UseShellExecute = false
                    },
                    EnableRaisingEvents = true
                };

                p.Exited += (sender, args) => {
                    done = true;
                };

                try {
                    p.Start();

                    var elapsedTime = 0;

                    while (!done)
                    {
                        elapsedTime += SleepAmount;

                        if (elapsedTime > currentTimeout)
                        {
                            p.Kill();

                            return new TaskResult {
                                Type = TaskResult.TaskResultType.Timeout,
                                ErrorMessage = "Process is timeouted"
                            };
                        }

                        Thread.Sleep(SleepAmount);
                    }
                }
                catch (Exception e) {
                    //Already updated
                    if (FileName.StartsWith("wuauclt")) {
                        return new TaskResult { Type = TaskResult.TaskResultType.Ok };
                    }
                    else {
                        return new TaskResult {Type = TaskResult.TaskResultType.Error, ErrorMessage = e.Message};
                    }
                }

                return new TaskResult {Type = TaskResult.TaskResultType.Ok};
            });
        }
    }
}