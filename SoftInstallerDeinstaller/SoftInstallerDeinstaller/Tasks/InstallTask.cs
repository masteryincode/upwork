﻿using SoftInstallerDeinstaller.TaskConfig;

namespace SoftInstallerDeinstaller.Tasks {
    public class InstallTask: MyCommonTask {
        public override TaskType TaskType => TaskType.Install;
        public override string FileName { get; set; }
        public override string Arguments { get; set; }

        public static InstallTask Load(TaskConfigElement config) {
            return new InstallTask {Name = config.Name, FileName = config.Path, Arguments = config.Arguments, Timeout = config.Timeout};
        }
    }
}