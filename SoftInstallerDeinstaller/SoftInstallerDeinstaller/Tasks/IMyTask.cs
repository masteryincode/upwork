﻿using System.Threading.Tasks;

namespace SoftInstallerDeinstaller.Tasks {
    public interface IMyTask {
        string Name { get; set; }
        int Priority { get; }
        TaskType TaskType { get; }
        string FileName { get; set; }
        string Arguments { get; set; }
        int Timeout { get; set; } //sec

        Task<TaskResult> Create();
    }
}