using SoftInstallerDeinstaller.TaskConfig;

namespace SoftInstallerDeinstaller.Tasks {
    public class UninstallTask: MyCommonTask {
        public override TaskType TaskType => TaskType.Uninstall;
        public override string FileName { get; set; }
        public override string Arguments { get; set; }

        public static UninstallTask Load(TaskConfigElement config) {
            return new UninstallTask {Arguments = config.Arguments, FileName = config.Path, Name = config.Name, Timeout = config.Timeout };
        }
    }
}