﻿namespace SoftInstallerDeinstaller.Tasks {
    public class AddUserTask: MyCommonTask {
        public string Login { get; set; }
        public string Password { get; set; }
        public override TaskType TaskType => TaskType.AddUser;
        public override string FileName {
            get { return @"net"; }
            set { }
        }

        public override string Arguments {
            get { return $@"user {Login} {Password} /add"; }
            set { }
        }
    }
}