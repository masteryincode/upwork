﻿using SoftInstallerDeinstaller.TaskConfig;

namespace SoftInstallerDeinstaller.Tasks {
    public class OtherTask : MyCommonTask {
        public override TaskType TaskType => TaskType.Other;
        public override string FileName { get; set; }
        public override string Arguments { get; set; }

        public static OtherTask Load(TaskConfigElement config) {
            return new OtherTask {Arguments = config.Arguments, FileName = config.Path, Name = config.Name, Timeout = config.Timeout };
        }
    }
}