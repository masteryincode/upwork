﻿using SoftInstallerDeinstaller.TaskConfig;

namespace SoftInstallerDeinstaller.Tasks {
    public class UpdateTask : MyCommonTask {
        public override TaskType TaskType => TaskType.Update;
        public override string FileName { get; set; }
        public override string Arguments { get; set; }

        public static UpdateTask Load(TaskConfigElement config) {
            return new UpdateTask {Arguments = config.Arguments, FileName = config.Path, Name = config.Name, Timeout = config.Timeout };
        }
    }
}