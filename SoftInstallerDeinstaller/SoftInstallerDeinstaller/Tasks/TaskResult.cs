﻿namespace SoftInstallerDeinstaller.Tasks {
    public class TaskResult {
        public enum TaskResultType {
            Error, Timeout, Ok
        }
        
        public TaskResultType Type { get; set; }
        public string ErrorMessage { get; set; }
    }
}