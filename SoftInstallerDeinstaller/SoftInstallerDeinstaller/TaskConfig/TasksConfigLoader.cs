using System.Collections.Generic;
using SoftInstallerDeinstaller.Tasks;

namespace SoftInstallerDeinstaller.TaskConfig {
    public class TasksConfigLoader {
        public Dictionary<TaskType, List<IMyTask>> LoadTasks() {
            var result = new Dictionary<TaskType, List<IMyTask>> {
                {TaskType.AddUser, new List<IMyTask> {new AddUserTask {Name = @"Add User"}}}
            };

            foreach (TaskConfigElement taskConfig in TaskConfigSection.GetConfig().Tasks) {
                switch (taskConfig.Type) {
                    case TaskType.AddUser:
                        break;

                    case TaskType.Install:
                        var installTask = InstallTask.Load(taskConfig);

                        if (result.ContainsKey(TaskType.Install)) {
                            result[TaskType.Install].Add(installTask);
                        }
                        else {
                            result.Add(TaskType.Install, new List<IMyTask> {installTask});
                        }

                        break;

                    case TaskType.Uninstall:
                        var uninstallTask = UninstallTask.Load(taskConfig);

                        if (result.ContainsKey(TaskType.Uninstall)) {
                            result[TaskType.Uninstall].Add(uninstallTask);
                        }
                        else {
                            result.Add(TaskType.Uninstall, new List<IMyTask> {uninstallTask});
                        }

                        break;

                        case TaskType.Update:
                        var updateTask = UpdateTask.Load(taskConfig);

                        if (result.ContainsKey(TaskType.Update))
                        {
                            result[TaskType.Update].Add(updateTask);
                        }
                        else {
                            result.Add(TaskType.Update, new List<IMyTask> { updateTask });
                        }

                        break;

                        case TaskType.Other:
                        var otherTask = OtherTask.Load(taskConfig);

                        if (result.ContainsKey(TaskType.Other))
                        {
                            result[TaskType.Other].Add(otherTask);
                        }
                        else {
                            result.Add(TaskType.Other, new List<IMyTask> { otherTask });
                        }

                        break;
                    default:
                        break;
                }
            }

            return result;
        }
    }
}