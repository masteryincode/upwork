﻿using System.Configuration;

namespace SoftInstallerDeinstaller.TaskConfig {
    [ConfigurationCollection(typeof(TaskConfigElement), AddItemName = "task")]
    public class TaskConfigCollection: ConfigurationElementCollection {
        public TaskConfigElement this[int index] {
            get {
                return BaseGet(index) as TaskConfigElement;
            }

            set {
                if (BaseGet(index) != null) {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }


        protected override ConfigurationElement CreateNewElement() {
            return new TaskConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((TaskConfigElement)element).Name;
        }
    }
}