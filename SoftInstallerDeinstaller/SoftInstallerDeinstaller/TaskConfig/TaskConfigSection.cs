﻿using System.Configuration;

namespace SoftInstallerDeinstaller.TaskConfig {
    public class TaskConfigSection: ConfigurationSection {
        private const string Name = "tasksConfig";

        public static TaskConfigSection GetConfig() {
            return (TaskConfigSection) ConfigurationManager.GetSection(Name) ?? new TaskConfigSection();
        }

        [ConfigurationProperty("tasks")]
        public TaskConfigCollection Tasks => (TaskConfigCollection) this["tasks"] ?? new TaskConfigCollection();
    }
}