﻿using System.Configuration;
using SoftInstallerDeinstaller.Tasks;

namespace SoftInstallerDeinstaller.TaskConfig {
    public class TaskConfigElement: ConfigurationElement {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name => this["name"] as string;

        [ConfigurationProperty("type", IsRequired = true, DefaultValue = TaskType.Install)]
        public TaskType Type => (TaskType)this["type"];

        [ConfigurationProperty("path", IsRequired = true)]
        [StringValidator(InvalidCharacters = "*?|<>")]
        public string Path => this["path"] as string;

        [ConfigurationProperty("args", IsRequired = false)]
        public string Arguments => this["args"] as string;

        //timeout in seconds
        [ConfigurationProperty("timeout", IsRequired = false, DefaultValue = 600)]
        public int Timeout => (int)this["timeout"];
    }
}