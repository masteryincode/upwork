﻿using System;
using System.Windows.Forms;
using SoftInstallerDeinstaller.TaskConfig;

namespace SoftInstallerDeinstaller {
    internal static class Program {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new MainForm(new TasksConfigLoader().LoadTasks()));
        }
    }
}