﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SoftInstallerDeinstaller.Tasks;

namespace SoftInstallerDeinstaller {
    public partial class MainForm : Form {
        private readonly TreeNode _addUserNode;
        private readonly Dictionary<string, IMyTask> _allTasks = new Dictionary<string, IMyTask>();
        private readonly TreeNode _installNode;
        private readonly TreeNode _otherNode;
        private readonly TreeNode _uninstallNode;
        private readonly TreeNode _updateNode;

        public MainForm(Dictionary<TaskType, List<IMyTask>> tasks) {
            InitializeComponent();

            var rootNode = tasksTreeViewer.Nodes[0];

            tasksTreeViewer.BeginUpdate();
            foreach (var taskType in tasks.Keys) {
                switch (taskType) {
                    case TaskType.AddUser:
                        _addUserNode = new TreeNode(@"Add User");
                        rootNode.Nodes.Add(_addUserNode);
                        _allTasks.Add(@"Add User", tasks[taskType].First());
                        break;

                    case TaskType.Uninstall:
                        _uninstallNode = new TreeNode(@"Uninstall");

                        foreach (var task in tasks[taskType]) {
                            _uninstallNode.Nodes.Add(new TreeNode {Text = task.Name});
                            _allTasks.Add(task.Name, task);
                        }

                        rootNode.Nodes.Add(_uninstallNode);
                        break;

                    case TaskType.Install:
                        _installNode = new TreeNode(@"Install");

                        foreach (var task in tasks[taskType])
                        {
                            _installNode.Nodes.Add(new TreeNode { Text = task.Name });
                            _allTasks.Add(task.Name, task);
                        }

                        rootNode.Nodes.Add(_installNode);
                        break;

                    case TaskType.Update:
                        _updateNode = new TreeNode(@"Update");

                        foreach (var task in tasks[taskType])
                        {
                            _updateNode.Nodes.Add(new TreeNode { Text = task.Name });
                            _allTasks.Add(task.Name, task);
                        }

                        rootNode.Nodes.Add(_updateNode);
                        break;

                    case TaskType.Other:
                        _otherNode = new TreeNode(@"Other");

                        foreach (var task in tasks[taskType])
                        {
                            _otherNode.Nodes.Add(new TreeNode { Text = task.Name });
                            _allTasks.Add(task.Name, task);
                        }

                        rootNode.Nodes.Add(_otherNode);
                        break;
                }
            }
            tasksTreeViewer.CheckBoxes = true;
            tasksTreeViewer.ExpandAll();
            tasksTreeViewer.EndUpdate();
        }

        private void nextButton1_Click(object sender, EventArgs e) {
            tabs.SelectedIndex++;
        }

        private void prevButton1_Click(object sender, EventArgs e) {
            tabs.SelectedIndex--;
        }

        private void tabs_SelectedIndexChanged(object sender, EventArgs e) {
            if (_addUserNode != null) {
                addUserGroupBox.Visible = _addUserNode.Checked;
            }
        }

        private List<IMyTask> CollectCheckedTasks() {
            var result = new List<IMyTask>();

            if (_addUserNode != null) {
                if (_addUserNode.Checked) {
                    result.Add(_allTasks[_addUserNode.Text]);
                }
            }

            if (_uninstallNode != null) {
                result.AddRange(from TreeNode subNode in _uninstallNode.Nodes where subNode.Checked select _allTasks[subNode.Text]);
            }

            if (_installNode != null)
            {
                result.AddRange(from TreeNode subNode in _installNode.Nodes where subNode.Checked select _allTasks[subNode.Text]);
            }

            if (_updateNode != null)
            {
                result.AddRange(from TreeNode subNode in _updateNode.Nodes where subNode.Checked select _allTasks[subNode.Text]);
            }

            if (_otherNode != null)
            {
                result.AddRange(from TreeNode subNode in _otherNode.Nodes where subNode.Checked select _allTasks[subNode.Text]);
            }

            result.Sort((t1, t2) => {
                var taskTypeDifference = (int) t1.TaskType - (int) t2.TaskType;

                if (taskTypeDifference < 0) {
                    return -1;
                }

                return taskTypeDifference == 0 ? string.Compare(t1.Name, t2.Name, StringComparison.CurrentCulture) : 1;
            });

            return result;
        }

        private void startButton_Click(object sender, EventArgs e) {
            var tasks = CollectCheckedTasks();
            
            Console.WriteLine(@"Collected tasks");
            tasks.ForEach(task => Console.WriteLine($"'{task.Name}'"));

            if (addUserGroupBox.Visible &&
                (loginTextBox.Text.Trim().Length == 0 || passwordTextBox.Text.Trim().Length == 0)) {
                MessageBox.Show("Login or password is empty");
            }
            else {
                progressBar1.Maximum = tasks.Count;
                progressBar1.Value = 0;

                foreach (var task in tasks) {
                    if (task.TaskType == TaskType.AddUser) {
                        var addUserTask = (AddUserTask) task;

                        addUserTask.Login = loginTextBox.Text.Trim();
                        addUserTask.Password = passwordTextBox.Text.Trim();
                    }

                    var systemTask = task.Create();

                    progressBar1.Text = task.Name;
                    startButton.Enabled = false;
                    startButton.Text = $@"STARTED [{task.Name}]";

                    systemTask.Start();

                    var result = systemTask.Result;

                    if (result.Type == TaskResult.TaskResultType.Ok) {
                        progressBar1.Value += 1;
                    }
                    else {
                        MessageBox.Show($"Task '{task.Name}': '{result.ErrorMessage}'");
                        progressBar1.Value = 0;
                        startButton.Enabled = true;
                        startButton.Text = @"START";

                        return;
                    }

                    startButton.Enabled = true;
                    startButton.Text = @"START";
                }
            }
        }
    }
}