import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;


public class RotateTriangle extends JFrame {

    /**
     * The original x points.
     */
    private int[] xPoints = new int[]{0, 25, 50};

    /**
     * The original y points.
     */
    private int[] yPoints = new int[]{50, 0, 50};

    /**
     * The current x points.
     */
    private int[] currentXPts=new int[]{0, 25, 50};

    /**
     * The current y points.
     */
    private int[] currentYPts=new int[]{50, 0, 50};

    /**
     * The current global x points.
     */
    private int[] currentGlobalXPts=new int[]{0, 25, 50};

    /**
     * The current global y points.
     */
    private int[] currentGlobalYPts=new int[]{50, 0, 50};

    private boolean rotate;

    double angle = 0;

    /**
     * Instantiates a new draw triangle.
     */
    public RotateTriangle() {
        super("Rotate Triangle");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(new RotateTrianglePanel());
        pack();
        setLocationRelativeTo(null);
        setVisible(true);

    }

    /**
     * The Class DrawTriadPanel.
     */
    private class RotateTrianglePanel extends JPanel implements MouseListener,
            MouseMotionListener {

        private Point clickPoint;

        /**
         * Instantiates a new draw triangle panel.
         */
        public RotateTrianglePanel() {
            addMouseListener(this);
            addMouseMotionListener(this);

            //triangle centre points
            clickPoint = new Point(100, 100);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(200, 200);
        }

        /**
         * Gets the height and width of triangle.
         */
        protected Dimension getTriangleSize() {
            int maxX = 0;
            int maxY = 0;
            for (int index = 0; index < xPoints.length; index++) {
                maxX = Math.max(maxX, xPoints[index]);
            }
            for (int index = 0; index < yPoints.length; index++) {
                maxY = Math.max(maxY, yPoints[index]);
            }
            return new Dimension(maxX, maxY);
        }


        /**
         * Drawing the triangle.
         *
         * @param g the g
         * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Dimension size = getTriangleSize();

            final Point2D.Float centerOfMass = new Point2D.Float(
                    (xPoints[0] + xPoints[1] + xPoints[2]) / 3.0f,
                    (yPoints[0] + yPoints[1] + yPoints[2]) / 3.0f);

            final Point2D.Float centerOfFrame = new Point2D.Float(
                    size.width / 2.0f,
                    size.height / 2.0f);

            // Center of Mass or Center of Frame?
            final Point2D.Float center = centerOfMass;

            final double angleInRadians = Math.toRadians(angle);
            final double sinA = Math.sin(angleInRadians);
            final double cosA = Math.cos(angleInRadians);
            final int globalCenterX = clickPoint.x - (int)center.x;
            final int globalCenterY = clickPoint.y - (int)center.y;

            for (int index = 0; index < xPoints.length; index++) {
                // X' = (X - Xc) * cos(A) - (Y - Yc) * sin(A) + Xc
                // Y' = (X - Xc) * sin(A) + (Y - Yc) * cos(A) + Yc

                final int dx = xPoints[index] - (int)center.x;
                final int dy = yPoints[index] - (int)center.y;


                currentXPts[index] = (int)(dx * cosA - dy * sinA + center.x);
                currentYPts[index] = (int)(dx * sinA + dy * cosA + center.y);

                currentGlobalXPts[index] = currentXPts[index] + globalCenterX;
                currentGlobalYPts[index] = currentYPts[index] + globalCenterY;
            }

            Graphics2D g2d = (Graphics2D) g.create();
            g2d.drawPolygon(currentGlobalXPts, currentGlobalYPts, 3);
            g2d.setColor(Color.RED);
            g2d.drawLine(currentGlobalXPts[1], currentGlobalYPts[1], clickPoint.x, clickPoint.y);
            g2d.dispose();
        }


        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseListener#mousePressed (java.awt.event.MouseEvent)
         */
        @Override
        public void mousePressed(MouseEvent e) {
            System.out.println("Mouse pressed called");
            recalculateAngle(e);
            repaint();

        }

        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseListener#mouseReleased (java.awt.event.MouseEvent)
         */
        @Override
        public void mouseReleased(MouseEvent e) {
            System.out.println("Mouse released called");
        }

        /**
         * (non-Javadoc).
         *
         * Calcualtes the angle of rotation
         *
         * @param e the e
         * @see java.awt.event.MouseMotionListener#mouseDragged (java.awt.event.MouseEvent)
         */
        public void mouseDragged(MouseEvent e) {
            System.out.println("Mouse dragged called");

            recalculateAngle(e);
            repaint();
        }

        private void recalculateAngle(MouseEvent e) {
            int x = e.getPoint().x - clickPoint.x;
            int y = e.getPoint().y - clickPoint.y;
            angle = Math.atan2(y,x)  * 180 / Math.PI;
            angle += (90);
        }

        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseListener#mouseEntered (java.awt.event.MouseEvent)
         */
        public void mouseEntered(MouseEvent e) {
            System.out.println("Mouse Entered.");
        }

        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseListener#mouseExited (java.awt.event.MouseEvent)
         */
        public void mouseExited(MouseEvent e) {
            System.out.println("Mouse exited.");
        }

        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseListener#mouseClicked (java.awt.event.MouseEvent)
         */
        public void mouseClicked(MouseEvent e) {
        }

        /**
         * (non-Javadoc).
         *
         * @param e the e
         * @see java.awt.event.MouseMotionListener#mouseMoved (java.awt.event.MouseEvent)
         */
        public void mouseMoved(MouseEvent e) {
        }

    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                new RotateTriangle();
            }
        });
    }

}